#Main class
import sys
import matplotlib.pyplot as plt
import reader
from Thalamus import Thalamus
from orbitoFCortex import OrbitofrontalCortex
from SensoryCortex import SensoryCortex
from Amygdala import Amygdala
import extraData
import numpy as np
import os

path=os.path.dirname(os.path.abspath(__file__))
print(path)

alpha=extraData.alpha
beta=extraData.beta

#--------------------------- file reading ----------------------#

simuName='acquisition'
#simuName='blocking'
#simuName='conditionedInhibition'

if(len(sys.argv)>1):
	simuName=sys.argv[1]
title=simuName
simuName = path+'/simulations/'+simuName+'.txt';
print ("Opening file : ",simuName)
myInputs=reader.readFile(simuName)

displayWeights=False
if(len(sys.argv)>2):
	displayWeights=(sys.argv[2] == "w")
frequence=0.06

#listes
SignalReward=[]
OutputE=[]
Vth=[]
VO=[]
WO=[]
SignalThalamus=[]
SignalInputS=[]
SignalReward=[]

#--------------------------- data initialisation ----------------------#

taille =myInputs[0].size
# Entries est un dictionnaire associant le nom d'une entree (input) et la liste de ses valeurs
entries = {}
SignalThalamus = [0]*taille
# inputsIndices contiendra les indices auxquelle un certain input sera actif (= 1).
# On considere donc que pour tout pas de temps n'appartenant pas a cette liste, l'entree sera nulle.
inputsIndices={}
inputsIndices["Thalamus"]=[]
for i in myInputs:
	thal = [0] * taille
	inputsIndices[i.name]=[0]
	for p in i.phases:
		if(p.frequence>frequence):
			frequence=p.frequence
		step = (int)(1/p.frequence	)
		for j in np.arange(p.start,p.end,step):
			thal[j]=1
			inputsIndices[i.name].append(j)
			if (i.name != "Reward"):
				inputsIndices["Thalamus"].append(j)
					
	entries[i.name]=thal
	inputsIndices[i.name].append(taille)


entries["Thalamus"]= SignalThalamus
SignalReward=entries["Reward"]
# On separe les signal inputs des autres entrees.
for e in reversed((list)(entries.keys())):
	if (e != "Reward" and e!= "Thalamus"):
		SignalInputS.append(entries.get(e))

inputsIndices["Thalamus"].append(taille)
inputsNb =len(SignalInputS)

#Initialisation des deux parties communicantes
amygdala=Amygdala(alpha,inputsNb)
orbitoFCortex=OrbitofrontalCortex(beta,inputsNb)
sensoryCortex = SensoryCortex()
thalamus= Thalamus()
# Initialisation de l'abscisse en fonction du pas de temps le plus petit 
#(selon les frequence des inputs)
abscisse=np.arange(0,taille,(int)(1/frequence))
#--------------------------- loop ----------------------#
previousE=0

for i in abscisse:
	Si= []
	# On recupere les differents signaux (a t=i)
	for s in SignalInputS :
		Si.append(s[i])

	rew=SignalReward[i]
	
	Si=thalamus.Step(Si)
	Si=sensoryCortex.Step(Si)
	SignalThalamus[i]=thalamus.getMax()



	#SignalThalamus[i]=max(Si)

	#Chaque partie retourne une sortie
	sumANodes = amygdala.Step(Si,SignalThalamus[i],rew)
	sumONodes = orbitoFCortex.Step(Si,sumANodes,rew,previousE)

	# On stocke les poids (pour l'affichage seulement)
	WO.append(orbitoFCortex.GetWO())
	VO.append(amygdala.GetListVO())
	Vth.append(amygdala.GetListVth())

	# Calcul du output E
	E = sumANodes - sumONodes
	if(E<0):
		E=0

	OutputE.append(E)
	previousE = E

#--------------------------- plot inputs ----------------------#
plt.figure(1)

indice=0
outputNb = 1
if (displayWeights):
	outputNb +=3

for e in entries.keys():
	indice+=1
	plt.subplot(len(entries)+outputNb,1,len(entries)+1-indice) #Pour inverser le sens
	plt.vlines(inputsIndices.get(e), 0, 1)
	plt.ylabel(e)
	#Masque axe des abscisses
	plt.tick_params(
	   axis='x',         
	   which='both',      
	   bottom=False,   
	   top=False, 
	   labelbottom=False)


#--------------------------- plot outputs ----------------------#
plt.suptitle(title, fontsize=16)
x = abscisse
y = OutputE

plt.subplot(len(entries)+outputNb,1,len(entries)+1)
plt.step(x, y, label='pre (default)')
plt.ylabel('E')
if(displayWeights):
	plt.tick_params(
		   axis='x',          
		   which='both',   
		   bottom=False, 
		   top=False,     
		   labelbottom=False)

#--------------------------- display weights ----------------------#
if(displayWeights):
	plt.subplot(len(entries)+outputNb,1,len(entries)+2) 
	y = Vth
	plt.step(x, y, label='pre (default)')
	plt.ylabel('Vth')
	plt.tick_params(
		   axis='x',    
		   which='both',  
		   bottom=False,
		   top=False, 
		   labelbottom=False)

	plt.subplot(len(entries)+outputNb,1,len(entries)+3)
	y = VO
	plt.step(x, y, label='pre (default)')
	plt.ylabel('VO')
	plt.tick_params(
		   axis='x',  
		   which='both', 
		   bottom=False,
		   top=False,
		   labelbottom=False)

	plt.subplot(len(entries)+outputNb,1,len(entries)+4)
	y = WO
	plt.step(x, y, label='pre (default)')
	plt.ylabel('WO')
plt.show()


