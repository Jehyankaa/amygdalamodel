# AmygdalaModel

UE : Intelligence Artificielle pour la Robotique (UPMC)

Reproduction partielle du modèle computationnel de l'apprentissage émotionnel dans l'amygdale décrit dans l'article suivant : A Computational Model of Emotional Learning in the Amygdala, J.Moren and C.Balkernius.

Trois fichiers décrivant les inputs des différentes simulations sont fournis dans le dossier ./simulations.
Pour lancer le modèle, il faut rajouter le nom de la simulation comme argument. (acquisition, blocking ou conditionedInhibition). 
L'affichage des poids est optionnel. On peut l'activer en rajoutant un second argument "w"

Exemple : $python main.py blocking [Pour afficher le résultat de la simulation 'blocking']
$python main.py acquisition w [Pour afficher le résultat de la simulation 'acquisition' avec la courbe des poids]