import numpy as np
# Rend seulement le max des inputs en sortie (pour l'instant)
class Thalamus:

    def __init__(self):
        self.sInputs=list()

    def getMax(self):
        return max(self.sInputs)

    def Step(self, listInput):
    	self.sInputs=listInput
    	return self.sInputs

    def GetSignaux(self):
        return self.sInputs






