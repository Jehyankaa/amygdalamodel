##TO READ FILES
from Thalamus import Thalamus
from orbitoFCortex import OrbitofrontalCortex
from SensoryCortex import SensoryCortex
from Amygdala import Amygdala
import sys
import matplotlib.pyplot as plt

class SensoryInput:#renvoi un objet liste de la forme [start,end,freq] et nom (thalamus ..)
	def __init__(self,name,size,phases):
		self.name=name
		self.size=int(size)
		self.phases=list()
		for p in phases:
			self.phases.append(Phase(p[0],p[1],p[2]))


class Phase:
	def __init__(self,start,end,frequence):
		self.start=int(start)
		self.end=int(end)
		self.frequence=float(frequence)


myInputs=list()
def saveInput(name,size,attributs):
	global myInputs
	phases=list()
	i=0
	while(i+3<=len(attributs)):
		phases.append(attributs[i:i+3])
		i+=3

	sI=SensoryInput(name,size,phases)
	myInputs.append(sI)

def display():
	global myInputs
	print("Mes objects input : ")
	for i in myInputs:
		print("NOM : " , i.name)
		print("Les Phases : ")
		for p in i.phases:
			print("Intervalle ", p.start, p.end)
			print("Frequence : ", p.frequence)

def readFile(title):
	f=open(title,'r+')
	name=""
	attributs=list()
	size=f.readline().split('=')[1]
	for l in f.readlines() :
		l=l.strip('\n')
		if(len(l)>0):
			if(l[0] is "#"):
				if(len(name)>0):
					saveInput(name,size,attributs)
				#Sauvegarde du input precedent
				name=l[1:]
				attributs=list()
				##NEW ITEM
			else:
				attributs.append(l.split("=")[1])

	saveInput(name,size,attributs)
	#display()
	return myInputs







