#Cortex orbitofrontal
import numpy as np

class OrbitofrontalCortex :
  def __init__(self, beta, inputsNb):
    self.beta = beta
    self.ListeO=[0] * (inputsNb)
    self.ListeW=[0] * (inputsNb)

  def Step(self,Si,SumANodes,Reward,EPreviousStep):
    
    if (Reward == 0) :
      Ro = max(0,EPreviousStep)
    else:
      Ro = max(0,SumANodes - Reward) - sum(self.ListeO)

    deltaW = list(map((lambda x : x * self.beta * Ro),Si))
    self.ListeW= list(x+y for x,y in zip(self.ListeW,deltaW))

    for i in range(len(self.ListeW)):
      if(self.ListeW[i]>1):
        self.ListeW[i]=1
      if(self.ListeW[i]<0):
        self.ListeW[i]=0 
    self.ListeO= list(x*y for x,y in zip(self.ListeW,Si)) 

    return np.sum(self.ListeO)

  def getNodes(self):
    return self.ListeO 

  def GetListeW(self): 
    return self.ListeW

  def GetWO(self): 
    return self.ListeW[0]

  