#Main class
import sys
import matplotlib.pyplot as plt
import reader
from Thalamus import Thalamus
from orbitoFCortex import OrbitofrontalCortex
from SensoryCortex import SensoryCortex
from Amygdala import Amygdala
import extraData
import numpy as np

alpha=extraData.alpha
beta=extraData.beta

#--------------------------- file reading ----------------------#
simuName='blocking'
if(len(sys.argv)>1):
	simuName=sys.argv[1]
simuName = 'simulations/'+simuName+'.txt';
print ("Opening file : ",simuName)
myInputs=reader.readFile(simuName)



#listes
SignalReward=[]
OutputE=[]
Vth=[] #poids liee a la connexion thalamus
VO=[]
WO=[]

##apelle constructeur de nos classes et initialistation des parametre poid et noeud(A,O) pour orbito
Onodes=[]
weightsO=[]
#thalamus=Thalamus()

#sensoryCortex=SensoryCortex()


#--------------------------- data ----------------------#
SignalThalamus=[]
SignalInputS=[]
SignalReward=[]

taille =myInputs[0].size
entries = {}
SignalThalamus = [0]*taille


for i in myInputs:
	thal=[0]*taille
	for p in i.phases:
		step = (int)(1/p.frequence	)
		for ps in range(p.start,p.end):
		#for ps in np.arange(p.start,p.end,step):
			thal[ps]=1
			SignalThalamus[ps]=1
			
	entries[i.name]=thal

entries["Thalamus"]= SignalThalamus
SignalReward=entries["Reward"]
for e in entries.keys():
	if (e != "Reward" and e!= "Thalamus"):
		SignalInputS.append(entries.get(e))


inputsNb =len(SignalInputS)
amygdala=Amygdala(alpha,inputsNb)
orbitoFCortex=OrbitofrontalCortex(beta,inputsNb)
#abscisse=np.arange(0,taille,(int)(1/0.06))
abscisse=range(taille)
#--------------------------- loop ----------------------#

for i in abscisse:
	Si= []
	for s in SignalInputS :
		Si.append(s[i])
	rew=SignalReward[i]
	Si.append(SignalThalamus[i])
	sumANodes = amygdala.Step(Si,rew)

	previousE=0
	if(i>0):
		previousE = OutputE[i-1]

	sumONodes = orbitoFCortex.Step(Si,rew,previousE)

	#SensoryCortex.Step(thalamus.GetSignaux())#suivre le principe du shema sensory recupere ses donnee de thalamus
	#SensoryCortex.GetSignals().append(thalamus.getMax())# concatenne liste de sensory et la valeur de thalamus pour avoir [so,s1.s2,th]

	#orbitoFCortex.Step(SensoryCortex.GetSignals().append(thalamus.getMax()),reward,0,Amygdala.get.GetE())

	WO.append(orbitoFCortex.GetListeW())
	VO.append(amygdala.GetListVO())
	Vth.append(amygdala.GetListVth())
	OutputE.append(sumANodes - sumONodes)

#--------------------------- plot inputs ----------------------#
plt.figure(1)
indice=0
#SignalThalamus = [0]*taille
for e in entries.keys():
	indice+=1
	plt.subplot(len(entries),1,len(entries)-indice+1) #Pour inverser le sens
	#plt.bar(abscisse,entries.get(e),0)
	plt.vlines(entries.get(e), 0, 1)
	#plt.plot(abscisse,entries.get(e))
	plt.ylabel(e)
	#Unless it's the last plot, hide x axis ticks because it's moche
	if(indice<len(entries)):
		plt.tick_params(
	    axis='x',          # changes apply to the x-axis
	    which='both',      # both major and minor ticks are affected
	    bottom=False,      # ticks along the bottom edge are off
	    top=False,         # ticks along the top edge are off
	    labelbottom=False)
plt.show()

#--------------------------- plot outputs ----------------------#
x = abscisse
y = OutputE
plt.step(x, y, label='pre (default)')
plt.ylabel('E')
plt.show()

def displayWeights():
	displayVth
	displayVO
	displayWO

def displayVth():
	y = Vth
	plt.step(x, y, label='pre (default)')
	plt.ylabel('Vth')
	plt.show()

def displayVO():
	y = VO
	plt.step(x, y, label='pre (default)')
	plt.ylabel('VO')
	plt.show()

def displayWO():
	y = WO
	plt.step(x, y, label='pre (default)')
	plt.ylabel('WO')
	plt.show()
