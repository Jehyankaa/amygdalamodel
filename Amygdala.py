import numpy as np
import operator
class Amygdala:
    def __init__(self,Alpha, inputsNb):
        self.Alpha=Alpha
        self.ListA=[0] * (inputsNb + 1) 
        self.ListV=[0] * (inputsNb +1) 
        
    def CalculDeltaV(self,Si,Reward):
       
       sumAj = sum(self.ListA)
       MaxRewMoinsAj=max(0,(Reward-sumAj)) 

       DeltaV=list(map((lambda x : x *self.Alpha * MaxRewMoinsAj),Si))
       return DeltaV

    def CalculV(self,D):
        self.ListV=list(x+y for x,y in zip(self.ListV,D))
        return self.ListV
    
    def getSumA(self):
        return sum(self.ListA)

    def GetListA(self):
        return self.ListA
    
    def GetListVO(self):
        return self.ListV[0]
    
    def GetListVth(self):
        return self.ListV[len(self.ListV)-1]

    def GetListV(self):
        return self.ListV
    
    def Step(self,Si,Sth,Reward):
        Si.append(Sth)
        DeltaV=self.CalculDeltaV(Si,Reward)
        self.CalculV(DeltaV)
        self.ListA=list(x*y for x,y in zip(self.ListV,Si))
        Si.remove(Sth)
        return np.sum(self.ListA) 





        
