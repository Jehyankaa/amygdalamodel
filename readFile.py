##TO READ FILES
from Thalamus import Thalamus
from orbitoFCortex import OrbitofrontalCortex
from SensoryCortex import SensoryCortex
from Amygdala import Amygdala
import matplotlib.pyplot as plt
class SensoryInput:#renvoi un objet liste de la forme [start,end,freq] et nom (thalamaus ..)
	def __init__(self,name,phases):
		self.name=name
		self.phases=list()
		for p in phases:
			self.phases.append(Phase(p[0],p[1],p[2]))


class Phase:
	def __init__(self,start,end,frequence):
		self.start=start
		self.end=end
		self.frequence=frequence

myInputs=list()
f=open('blocking.txt','r+')

def saveInput(name,attributs):
	global myInputs
	phases=list()
	i=0
	while(i+3<=len(attributs)):
		phases.append(attributs[i:i+3])
		i+=3

	sI=SensoryInput(name,phases)
	myInputs.append(sI)

def display():
	global myInputs
	print("Mes objects input : ")
	for i in myInputs:
		print("NOM : " , i.name)
		print("Les Phases : ")
		for p in i.phases:
			print("Intervalle ", p.start, p.end)

			print("Frequence : ", p.frequence)
	
name=""
attributs=list()

for l in f.readlines() :
	l=l.strip('\n')
	if(len(l)>0):
		if(l[0] is "#"):
			if(len(name)>0):
				saveInput(name,attributs)
			#Sauvegarde du input precedent
			name=l[1:]
			attributs=list()
			##NEW ITEM
		else:
			attributs.append(l.split("=")[1])

#print(name ,attributs)
saveInput(name,attributs)
display()

###----------------------------------apres stockage de donnees (lu dans les .txt )dans des listes--------------
## Simulation de Acquisition
alpha=0.2
beta=0.8
##les donnees que l'on veut visualiser dans le plot on les initialise tout d'abord
SignalReward=[]
SignalThalamus=[]
SignalInputS=[]
SignalSortieE=[]
SignalVth=[] #poid liee a la connexion thalamus
SignalVO=[]
SignalWO=[]
##apelle constructeur de nos classes et initialistation des parametre poid et noeud(A,O) pour orbito
Onodes=[]
weightsO=[]
Thalamus=Thalamus()
SensoryCortex=SensoryCortex()
Amygdala=Amygdala(alpha,SignalReward)
orbitoFCortex=OrbitofrontalCortex(beta,Onodes,SignalReward,weightsO)
## boucle sur les donnees recuperer en haut (je crois dans le saveinput ou attribus
## on a qqlechose de cette forme a chaque pas de temps (Donc a chaque lecture dune valeur de th ,S et Rew stocker
#while je parcours met donnee
#Thalamus.Step()










##affichage de plot
##thalamus
print(myInputs[0].name,myInputs[0].phases[0].end)
print(myInputs[1].name)
taille =int(myInputs[0].phases[0].end)
#testlistethalamus=[1,1,1,1,1,1,1,1,1,1,1,1] #exemple test
testlistethalamus1=[1]*taille
width=0.1
#print(taille)
plt.bar(range(taille),testlistethalamus1,width,color="black")
plt.ylabel('Thalamus')
plt.show()







